package com.bakaeva.tm.command.data.json;

import com.bakaeva.tm.api.service.IDomainService;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.constant.DataConstant;
import com.bakaeva.tm.dto.Domain;
import com.bakaeva.tm.enumerated.Role;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileInputStream;

public class DataJsonLoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-json-load";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from json file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_JSON);
        ObjectMapper mapper = new ObjectMapper();
        final Domain domain = mapper.readValue(fileInputStream, Domain.class);
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        fileInputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}