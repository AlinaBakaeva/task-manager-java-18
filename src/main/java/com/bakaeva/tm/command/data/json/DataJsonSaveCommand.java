package com.bakaeva.tm.command.data.json;

import com.bakaeva.tm.api.service.IDomainService;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.constant.DataConstant;
import com.bakaeva.tm.dto.Domain;
import com.bakaeva.tm.enumerated.Role;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataJsonSaveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-json-save";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Save data to json file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON SAVE]");
        final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);
        final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}