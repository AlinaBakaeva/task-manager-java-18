package com.bakaeva.tm.command.data.xml;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.constant.DataConstant;
import com.bakaeva.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public class DataXmlClearCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-xml-clear";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Remove xml data file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML CLEAR]");
        final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}