package com.bakaeva.tm.command.data.xml;

import com.bakaeva.tm.api.service.IDomainService;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.constant.DataConstant;
import com.bakaeva.tm.dto.Domain;
import com.bakaeva.tm.enumerated.Role;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.FileInputStream;

public class DataXmlLoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-xml-load";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from xml file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_XML);
        ObjectMapper mapper = new XmlMapper();
        final Domain domain = mapper.readValue(fileInputStream, Domain.class);
        fileInputStream.close();
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}