package com.bakaeva.tm.command.data.base64;

import com.bakaeva.tm.api.service.IDomainService;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.constant.DataConstant;
import com.bakaeva.tm.dto.Domain;
import com.bakaeva.tm.enumerated.Role;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public class DataBase64SaveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-base64-save";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Save data to base64 binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");
        final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);

        final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        final byte[] bytes = byteArrayOutputStream.toByteArray();
        final String base64 = new BASE64Encoder().encode(bytes);
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}