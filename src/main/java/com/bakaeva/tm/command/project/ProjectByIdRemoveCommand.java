package com.bakaeva.tm.command.project;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.entity.Project;
import com.bakaeva.tm.util.TerminalUtil;

public class ProjectByIdRemoveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}