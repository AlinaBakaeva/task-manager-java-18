package com.bakaeva.tm.command.project;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.entity.Project;
import com.bakaeva.tm.util.TerminalUtil;

public class ProjectByIndexRemoveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().removeByIndex(userId, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}