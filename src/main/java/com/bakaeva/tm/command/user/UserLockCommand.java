package com.bakaeva.tm.command.user;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-lock";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}