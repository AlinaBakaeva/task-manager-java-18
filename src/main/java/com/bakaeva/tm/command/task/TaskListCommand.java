package com.bakaeva.tm.command.task;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.entity.Task;

import java.util.List;

public class TaskListCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        for (final Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

}