package com.bakaeva.tm.command.task;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-create";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
    }

}