package com.bakaeva.tm.command.system;

import com.bakaeva.tm.bootstrap.Bootstrap;
import com.bakaeva.tm.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String argument() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (AbstractCommand command : commands) {
            System.out.printf("%-25s| %s\n", command.name(), command.description());
        }
        System.out.println("[OK]");
    }

}