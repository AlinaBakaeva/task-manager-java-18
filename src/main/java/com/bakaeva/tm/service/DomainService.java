package com.bakaeva.tm.service;

import com.bakaeva.tm.api.service.IDomainService;
import com.bakaeva.tm.api.service.IProjectService;
import com.bakaeva.tm.api.service.ITaskService;
import com.bakaeva.tm.api.service.IUserService;
import com.bakaeva.tm.dto.Domain;

public class DomainService implements IDomainService {

    private final IUserService userService;

    private final IProjectService projectService;

    private final ITaskService taskService;

    public DomainService(
            final IUserService userService,
            final IProjectService projectService,
            final ITaskService taskService
    ) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.findAll());
        domain.setUsers(userService.findAll());
        domain.setTasks(taskService.findAll());
    }

}