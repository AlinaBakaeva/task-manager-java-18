package com.bakaeva.tm.service;

import com.bakaeva.tm.api.service.IAuthService;
import com.bakaeva.tm.api.service.IUserService;
import com.bakaeva.tm.entity.User;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.exception.empty.EmptyLoginException;
import com.bakaeva.tm.exception.empty.EmptyPasswordException;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import com.bakaeva.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null || roles.length == 0) return;
        final String userId = getUserId();
        final User user = userService.findById(userId);
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) if (role.equals(item)) return;
        throw new AccessDeniedException();
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.isLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

}